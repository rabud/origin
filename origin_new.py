import json
import datetime
import requests

#Getting json from the file origin.json (from the server)
response = requests.get('http://[2804:14d:72b3:8c9b:6410:e3b6:112:9930]:80/origin.json')
person_json = json.loads(response.json())

#Rules that defines the profile
auto = 0
home =  0
disability = 0
life = 0
if person_json['income']==0:
    disability=99
else:
    if person_json['income']>200000:
        auto=auto-1
        home=home-1
        disability=disability-1
        life=life-1 
        
if person_json['vehicle']==0:
    auto=-99
    
if person_json['house']==0:
    home=99
else:
    if person_json['house']['ownership_status']=='mortgaged':
        home=home+1
        disability=disability+1
        
if person_json['age']>60:
    disability=99
    life=99
else:
    if person_json['age']<30:
        auto=auto-2
        home=home-2
        disability=disability-2
        life=life-2
    else:
        if person_json['age']<40:
            auto=auto-1
            home=home-1
            disability=disability-1
            life=life-1
   

if person_json['dependents']>0:
    disability=disability+1
    life=life+1    
    
if person_json['marital_status']=='married':
    disability=disability-1
    life=life+1   
    
difference = datetime.datetime.now().year-person_json['vehicle']['year']
if difference<=5:
    auto=auto+1


#Preparing the final json
final_json = {}
def define(value):
    if value==99:
        return 'ineligible'
    else:
        if value<=0:
            return 'economic'
        else:
            if value<=2:
                return 'regular'
            else:
                return 'responsible'
final_json.update(home=define(auto))
final_json.update(auto=define(home))
final_json.update(life=define(life))
final_json.update(disability=define(disability))

#Writing json in the file origin_output.json (local)
with open('origin_output.json', 'w') as outfile:
    json.dump(final_json, outfile)